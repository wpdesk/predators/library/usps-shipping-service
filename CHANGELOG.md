## [1.14.3] - 2024-11-19
### Fixed
- service name

## [1.14.2] - 2024-11-19
### Fixed
- service name

## [1.14.1] - 2024-11-15
### Fixed
- special services renamed to extra services
### Changed
- debug info for SKUs

## [1.14.0] - 2024-10-17
### Added
- Value of contents
### Changed
- Temporary disabled REST API

## [1.13.3] - 2024-10-14
### Changed
- MailType in WebTools API International changed from PACKAGE to ALL

## [1.13.2] - 2024-10-10
### Fixed
- Package defaults removed from global settings

## [1.13.1] - 2024-10-09
### Fixed
- Package weight calculation

## [1.13.0] - 2024-10-08
### Added
- REST API Services configuration

## [1.12.1] - 2024-07-01
### Fixed
- Currency verification

## [1.12.0] - 2024-06-14
### Added
- REST API

## [1.11.1] - 2024-03-06
### Fixed
- Insurance

## [1.11.0] - 2024-03-04
### Fixed
- Insurance

## [1.10.1] - 2024-01-29
### Fixed
- API password texts

## [1.10.0] - 2024-01-22
### Added
- API password

## [1.9.0] - 2023-11-28
### Added
- AS, GU, MP, VI, MH, FM, PW as domestic shipment

## [1.8.0] - 2023-10-13
### Added
- Puerto Rico as domestic shipment

## [1.7.0] - 2023-07-12
### Added
- Ground Advantage services:
    - 4058: USPS Ground Advantage HAZMAT
    - 1058: USPS Ground Advantage
    - 2058: USPS Ground Advantage Hold For Pickup
    - 6058: USPS Ground Advantage Parcel Locker
    - 4096: USPS Ground Advantage Cubic HAZMAT
    - 1096: USPS Ground Advantage Cubic
    - 2096: USPS Ground Advantage Cubic Hold For Pickup
    - 6096: USPS Ground Advantage Cubic Parcel Locker

## [1.6.1] - 2023-05-17
### Fixed
- API Settings texts

## [1.6.0] - 2023-03-01
### Added
- First Class services

## [1.5.3] - 2022-10-04
### Fixed
- weight calculations

## [1.5.1] - 2022-09-22
### Fixed
- post code length

## [1.5.0] - 2022-08-30
### Added
- de_DE translations

## [1.4.1] - 2022-08-16
### Fixed
- typo in service name

## [1.4.0] - 2022-08-15
### Added
- pl_PL translations

## [1.3.2] - 2022-07-11
### Fixed
- Fatal Error: Return value must be of type float, null returned

## [1.3.1] - 2022-05-12
### Fixed
- Texts
- Error handling: empty response

## [1.3.0] - 2022-04-19
### Changed
- Octolize urls

## [1.2.9] - 2021-11-10
### Fixed
- connection checker error when no User Id entered

## [1.2.8] - 2021-11-04
### Fixed
- services settings

## [1.2.7] - 2021-10-29
### Fixed
- currency verification

## [1.2.6] - 2021-10-25
### Changed
- commercial rates moved to rate adjustments

## [1.2.5] - 2021-10-20
### Changed
- labels and tooltips

## [1.2.4] - 2021-10-19
### Changed
- Multi currency exception message text

## [1.2.3] - 2021-10-15
### Fixed
- documentation link

## [1.2.2] - 2021-10-14
### Fixed
- countries: United Kingdom

## [1.2.1] - 2021-10-13
### Fixed
- cosmetics changes

## [1.2.0] - 2021-10-13
### Changed
- Removed custom origin

## [1.1.1] - 2021-10-12
### Fixed
- Debug info text duplication

## [1.1.0] - 2021-10-11
### Added
- Countries class

## [1.0.0] - 2021-10-04
### Added
- initial version
