[![pipeline status](https://gitlab.com/wpdesk/usps-shipping-service/badges/main/pipeline.svg)](https://gitlab.com/wpdesk/usps-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/usps-shipping-service/badges/main/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/usps-shipping-service/commits/main) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/usps-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/usps-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/usps-shipping-service/downloads)](https://packagist.org/packages/wpdesk/usps-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/usps-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/usps-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/usps-shipping-service/license)](https://packagist.org/packages/wpdesk/usps-shipping-service) 

USPS Shipping Service
====================

