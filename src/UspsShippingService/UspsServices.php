<?php

namespace WPDesk\UspsShippingService;

/**
 * A class that defines USPS services.
 */
class UspsServices {

    const API_SERVICES = [
        'PRIORITY COMMERCIAL',
        'PRIORITY MAIL CUBIC',
        'PRIORITY MAIL EXPRESS COMMERCIAL',
        'GROUND ADVANTAGE COMMERCIAL',
        'GROUND ADVANTAGE CUBIC',
        'MEDIA',
        'LIBRARY',
        'BPM',
    ];


    const FIRST_CLASS_LARGE_ENVELOPE = '0L';
    const FIRST_CLASS_PACKAGE_SERVICE_RETAIL = '0P';
    const FIRST_CLASS_PACKAGE_SERVICE_RETAIL_NAME = 'First-Class Package Service - Retail&lt;sup&gt;&#8482;&lt;/sup&gt;';
    const FIRST_CLASS_MAIL_LARGE_ENVELOPE_NAME = 'First-Class Mail&lt;sup&gt;&#174;&lt;/sup&gt; Large Envelope';
    /**
     * @return array
     */
    private function get_services(): array
    {
        return [
            'domestic'      => [
                self::FIRST_CLASS_LARGE_ENVELOPE         => __(
                    'First-Class Mail; Large Envelope',
                    'usps-shipping-service'
                ),
                self::FIRST_CLASS_PACKAGE_SERVICE_RETAIL => __(
                    'First-Class Package Service - Retail',
                    'usps-shipping-service'
                ),
                '1'                                      => __('Priority Mail', 'usps-shipping-service'),
                '2'                                      => __(
                    'Priority Mail Express; Hold For Pickup',
                    'usps-shipping-service'
                ),
                '3'                                      => __('Priority Mail Express', 'usps-shipping-service'),
                '4'                                      => __('Standard Post', 'usps-shipping-service'),
                '6'                                      => __('Media Mail', 'usps-shipping-service'),
                '7'                                      => __('Library Mail', 'usps-shipping-service'),
                '13'                                     => __(
                    'Priority Mail Express; Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '15'                                     => __(
                    'First-Class Mail; Large Postcards',
                    'usps-shipping-service'
                ),
                '16'                                     => __(
                    'Priority Mail; Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '17'                                     => __(
                    'Priority Mail; Medium Flat Rate Box',
                    'usps-shipping-service'
                ),
                '22'                                     => __(
                    'Priority Mail; Large Flat Rate Box',
                    'usps-shipping-service'
                ),
                '23'                                     => __(
                    'Priority Mail Express; Sunday/Holiday Delivery',
                    'usps-shipping-service'
                ),
                '25'                                     => __(
                    'Priority Mail Express; Sunday/Holiday Delivery Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '27'                                     => __(
                    'Priority Mail Express; Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '28'                                     => __(
                    'Priority Mail; Small Flat Rate Box',
                    'usps-shipping-service'
                ),
                '29'                                     => __(
                    'Priority Mail; Padded Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '30'                                     => __(
                    'Priority Mail Express; Legal Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '31'                                     => __(
                    'Priority Mail Express; Legal Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '32'                                     => __(
                    'Priority Mail Express; Sunday/Holiday Delivery Legal Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '33'                                     => __(
                    'Priority Mail; Hold For Pickup',
                    'usps-shipping-service'
                ),
                '34'                                     => __(
                    'Priority Mail; Large Flat Rate Box Hold For Pickup',
                    'usps-shipping-service'
                ),
                '35'                                     => __(
                    'Priority Mail; Medium Flat Rate Box Hold For Pickup',
                    'usps-shipping-service'
                ),
                '36'                                     => __(
                    'Priority Mail; Small Flat Rate Box Hold For Pickup',
                    'usps-shipping-service'
                ),
                '37'                                     => __(
                    'Priority Mail; Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '38'                                     => __(
                    'Priority Mail; Gift Card Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '39'                                     => __(
                    'Priority Mail; Gift Card Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '40'                                     => __(
                    'Priority Mail; Window Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '41'                                     => __(
                    'Priority Mail; Window Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '42'                                     => __(
                    'Priority Mail; Small Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '43'                                     => __(
                    'Priority Mail; Small Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '44'                                     => __(
                    'Priority Mail; Legal Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '45'                                     => __(
                    'Priority Mail; Legal Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '46'                                     => __(
                    'Priority Mail; Padded Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '47'                                     => __(
                    'Priority Mail; Regional Rate Box A',
                    'usps-shipping-service'
                ),
                '48'                                     => __(
                    'Priority Mail; Regional Rate Box A Hold For Pickup',
                    'usps-shipping-service'
                ),
                '49'                                     => __(
                    'Priority Mail; Regional Rate Box B',
                    'usps-shipping-service'
                ),
                '50'                                     => __(
                    'Priority Mail; Regional Rate Box B Hold For Pickup',
                    'usps-shipping-service'
                ),
                '53'                                     => __(
                    'First-Class; Package Service Hold For Pickup',
                    'usps-shipping-service'
                ),
                '55'                                     => __(
                    'Priority Mail Express; Flat Rate Boxes',
                    'usps-shipping-service'
                ),
                '56'                                     => __(
                    'Priority Mail Express; Flat Rate Boxes Hold For Pickup',
                    'usps-shipping-service'
                ),
                '57'                                     => __(
                    'Priority Mail Express; Sunday/Holiday Delivery Flat Rate Boxes',
                    'usps-shipping-service'
                ),
                '58'                                     => __(
                    'Priority Mail; Regional Rate Box C',
                    'usps-shipping-service'
                ),
                '59'                                     => __(
                    'Priority Mail; Regional Rate Box C Hold For Pickup',
                    'usps-shipping-service'
                ),
                '61'                                     => __('First-Class; Package Service', 'usps-shipping-service'),
                '62'                                     => __(
                    'Priority Mail Express; Padded Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '63'                                     => __(
                    'Priority Mail Express; Padded Flat Rate Envelope Hold For Pickup',
                    'usps-shipping-service'
                ),
                '64'                                     => __(
                    'Priority Mail Express; Sunday/Holiday Delivery Padded Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '78'   => __('First-Class Mail; Metered Letter', 'usps-shipping-service'),
                '84'   => __('Priority Mail; Cubic', 'usps-shipping-service'),
                '922'  => __('Priority Mail 1-Day Return Service Padded Flat Rate Envelope', 'usps-shipping-service'),
                '932'  => __(
                    'Priority Mail 1-Day Return Service Gift Card Flat Rate Envelope',
                    'usps-shipping-service'
                ),
                '934'  => __('Priority Mail 1-Day Return Service Window Flat Rate Envelope', 'usps-shipping-service'),
                '936'  => __('Priority Mail 1-Day Return Service Small Flat Rate Envelope', 'usps-shipping-service'),
                '938'  => __('Priority Mail 1-Day Return Service Legal Flat Rate Envelope', 'usps-shipping-service'),
                '939'  => __('Priority Mail 1-Day Return Service Flat Rate Envelope', 'usps-shipping-service'),
                '946'  => __('Priority Mail 1-Day Return Service Regional Rate Box A', 'usps-shipping-service'),
                '947'  => __('Priority Mail 1-Day Return Service Regional Rate Box B', 'usps-shipping-service'),
                '962'  => __('Priority Mail 1-Day Return Service', 'usps-shipping-service'),
                '963'  => __('Priority Mail 1-Day Return Service Large Flat Rate Box', 'usps-shipping-service'),
                '964'  => __('Priority Mail 1-Day Return Service Medium Flat Rate Box', 'usps-shipping-service'),
                '965'  => __('Priority Mail 1-Day Return Service Small Flat Rate Box', 'usps-shipping-service'),
                '966'  => __(
                    'Priority Mail Military Return Service Large Flat Rate Box APO/FPO/DPO',
                    'usps-shipping-service'
                ),
                '967'  => __('Priority Mail 1-Day Return Service Cubic', 'usps-shipping-service'),
                '968'  => __('First-Class Package Return Service', 'usps-shipping-service'),
                '969'  => __('Ground Return Service', 'usps-shipping-service'),
                '4058' => __('USPS Ground Advantage HAZMAT', 'usps-shipping-service'),
                '1058' => __('USPS Ground Advantage', 'usps-shipping-service'),
                '2058' => __('USPS Ground Advantage Hold For Pickup', 'usps-shipping-service'),
                '6058' => __('USPS Ground Advantage Parcel Locker', 'usps-shipping-service'),
                '4096' => __('USPS Ground Advantage Cubic HAZMAT', 'usps-shipping-service'),
                '1096' => __('USPS Ground Advantage Cubic', 'usps-shipping-service'),
                '2096' => __('USPS Ground Advantage Cubic Hold For Pickup', 'usps-shipping-service'),
                '6096' => __('USPS Ground Advantage Cubic Parcel Locker', 'usps-shipping-service'),
            ],
            'international' => [
                '1'  => __('Priority Mail Express International', 'usps-shipping-service'),
                '2'  => __('Priority Mail International', 'usps-shipping-service'),
                '4'  => __('Global Express Guaranteed; (GXG)**', 'usps-shipping-service'),
                '5'  => __('Global Express Guaranteed; Document', 'usps-shipping-service'),
                '6'  => __('Global Express Guarantee; Non-Document Rectangular', 'usps-shipping-service'),
                '7'  => __('Global Express Guaranteed; Non-Document Non-Rectangular', 'usps-shipping-service'),
                '8'  => __('Priority Mail International; Flat Rate Envelope**', 'usps-shipping-service'),
                '9'  => __('Priority Mail International; Medium Flat Rate Box', 'usps-shipping-service'),
                '10' => __('Priority Mail Express International; Flat Rate Envelope', 'usps-shipping-service'),
                '11' => __('Priority Mail International; Large Flat Rate Box', 'usps-shipping-service'),
                '12' => __('USPS GXG; Envelopes**', 'usps-shipping-service'),
                '13' => __('First-Class Mail; International Letter**', 'usps-shipping-service'),
                '14' => __('First-Class Mail; International Large Envelope**', 'usps-shipping-service'),
                '15' => __('First-Class Package International Service**', 'usps-shipping-service'),
                '16' => __('Priority Mail International; Small Flat Rate Box**', 'usps-shipping-service'),
                '17' => __('Priority Mail Express International; Legal Flat Rate Envelope', 'usps-shipping-service'),
                '18' => __('Priority Mail International; Gift Card Flat Rate Envelope**', 'usps-shipping-service'),
                '19' => __('Priority Mail International; Window Flat Rate Envelope**', 'usps-shipping-service'),
                '20' => __('Priority Mail International; Small Flat Rate Envelope**', 'usps-shipping-service'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function get_services_domestic(): array {
        return $this->get_services()['domestic'];
    }

    /**
     * @return array
     */
    public function get_services_international(): array {
        return $this->get_services()['international'];
    }

}
