<?php

namespace WPDesk\UspsShippingService;

class UspsSkuComponents
{

	public function get_service_types(): array {
		return [
			'P' => __( 'Priority Mail', 'usps-shipping-service' ),
			'E' => __( 'Priority Mail Express', 'usps-shipping-service' ),
			'M' => __( 'Media', 'usps-shipping-service' ),
			'F' => __( 'First-Class Mail', 'usps-shipping-service' ),
			'U' => __( 'USPS Ground Advantage*', 'usps-shipping-service' ),
			'L' => __( 'Library', 'usps-shipping-service' ),
		];
	}

	public function get_service_sub_types(): array  {
		return [
			'X' => __( 'None', 'usps-shipping-service' ),
			'A' => __( 'Automation', 'usps-shipping-service' ),
			'B' => __( 'Nonautomation', 'usps-shipping-service' ),
			'C' => __( 'Carrier Route', 'usps-shipping-service' ),
			'D' => __( 'Carrier Route Nonautomation', 'usps-shipping-service' ),
			'E' => __( 'Pending Periodicals', 'usps-shipping-service' ),
			'F' => __( 'Flat Rate', 'usps-shipping-service' ),
			'G' => __( 'USPS Connect Local', 'usps-shipping-service' ),
			'H' => __( 'USPS Connect Regional', 'usps-shipping-service' ),
			'I' => __( 'Irregular', 'usps-shipping-service' ),
			'K' => __( 'Share Mail', 'usps-shipping-service' ),
			'L' => __( 'Metered', 'usps-shipping-service' ),
			'M' => __( 'Machinable', 'usps-shipping-service' ),
			'N' => __( 'Nonmachinable', 'usps-shipping-service' ),
			'O' => __( 'USPS Connect Flat Rate', 'usps-shipping-service' ),
			'P' => __( 'Presorted', 'usps-shipping-service' ),
			'Q' => __( 'Automation Disc', 'usps-shipping-service' ),
			'R' => __( 'Regional Rate', 'usps-shipping-service' ),
			'S' => __( 'Simple Samples', 'usps-shipping-service' ),
			'T' => __( 'Permit Reply Mail', 'usps-shipping-service' ),
			'U' => __( 'Cubic', 'usps-shipping-service' ),
			'V' => __( 'Nonpresorted', 'usps-shipping-service' ),
			'W' => __( 'Permit Reply Mail', 'usps-shipping-service' ),
			'Y' => __( 'Nonautomation Disc', 'usps-shipping-service' ),
			'Z' => __( 'Customized', 'usps-shipping-service' ),
		];
	}

	public function get_shapes(): array {
		return [
			'X' => __( 'None', 'usps-shipping-service' ),
			'A' => __( 'Bag', 'usps-shipping-service' ),
			'B' => __( 'Box', 'usps-shipping-service' ),
			'C' => __( 'Postcards', 'usps-shipping-service' ),
			'E' => __( 'Envelope', 'usps-shipping-service' ),
			'F' => __( 'Flats or Large Envelope', 'usps-shipping-service' ),
			'H' => __( 'Half Tray', 'usps-shipping-service' ),
			'I' => __( 'Full Tray', 'usps-shipping-service' ),
			'J' => __( 'EMM Tray', 'usps-shipping-service' ),
			'K' => __( 'Tub', 'usps-shipping-service' ),
			'L' => __( 'Letters', 'usps-shipping-service' ),
			'M' => __( 'M-Bag', 'usps-shipping-service' ),
			'N' => __( 'Balloon', 'usps-shipping-service' ),
			'O' => __( 'Oversize', 'usps-shipping-service' ),
			'P' => __( 'Parcel or Package', 'usps-shipping-service' ),
			'Q' => __( 'Keys and IDs', 'usps-shipping-service' ),
			'R' => __( 'Dimensional Weight', 'usps-shipping-service' ),
			'U' => __( 'Pallet', 'usps-shipping-service' ),
			'V' => __( 'Half Pallet Box', 'usps-shipping-service' ),
			'W' => __( 'Full Pallet Box', 'usps-shipping-service' ),
		];
	}

	public function get_delivery_types(): array {
		return [
			'X' => __( 'None', 'usps-shipping-service' ),
			'H' => __( 'Hold for Pickup', 'usps-shipping-service' ),
			'S' => __( 'Sunday/Holiday', 'usps-shipping-service' ),
			'R' => __( 'Return', 'usps-shipping-service' ),
		];
	}

	public function add_codes_to_labels( array $array ): array {
		array_walk( $array, function( &$value, $key ) {
			$value = sprintf('(%1$s) %2$s', $key, $value );
		} );
		return $array;
	}

	public function get_filtered_array_by_codes( array $array, array $codes ): array {
		return array_filter( $array, function( $key ) use ( $codes ) {
			return in_array( $key, $codes, true );
		}, ARRAY_FILTER_USE_KEY );
	}

}
